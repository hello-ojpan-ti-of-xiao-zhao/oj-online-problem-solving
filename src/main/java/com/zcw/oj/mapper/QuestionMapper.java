package com.zcw.oj.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zcw.oj.model.entity.Question;

/**
* @author wei
* @description 针对表【question(题目)】的数据库操作Mapper
* @createDate 2023-09-25 17:02:08
* @Entity com.zcw.oj.service.impl.entity.Question
*/
public interface QuestionMapper extends BaseMapper<Question> {

}




