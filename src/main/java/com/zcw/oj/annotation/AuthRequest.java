package com.zcw.oj.annotation;

/**
 * 定义鉴权请求头和密钥
 *
 */
public interface AuthRequest {
    // 请求头
    String AUTH_REQUEST_HEADER = "auth";

    // 密钥 todo 需要加密
    String AUTH_REQUEST_SECRET = "secretKey";
}
