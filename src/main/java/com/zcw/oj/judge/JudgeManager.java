package com.zcw.oj.judge;

import com.zcw.oj.judge.strategy.DefaultJudgeStrategy;
import com.zcw.oj.judge.strategy.JavaLanguageJudgeStrategy;
import com.zcw.oj.judge.strategy.JudgeContext;
import com.zcw.oj.judge.strategy.JudgeStrategy;
import com.zcw.oj.judge.codesandbox.model.JudgeInfo;
import com.zcw.oj.model.entity.QuestionSubmit;
import org.springframework.stereotype.Service;

/**
 *  判题管理
 */
@Service
public class JudgeManager {

    JudgeInfo doJudge(JudgeContext judgeContext){
        QuestionSubmit questionSubmit = judgeContext.getQuestionSubmit();
        String language = questionSubmit.getLanguage();
        JudgeStrategy judgeStrategy = new DefaultJudgeStrategy();
        if ("java".equals(language)) {
            judgeStrategy = new JavaLanguageJudgeStrategy();
        }
        return judgeStrategy.doJudge(judgeContext);
    }
}
