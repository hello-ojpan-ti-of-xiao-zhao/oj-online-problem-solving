package com.zcw.oj.judge.codesandbox;

import com.zcw.oj.judge.codesandbox.model.ExecuteCodeRequest;
import com.zcw.oj.judge.codesandbox.model.ExecuteCodeResponse;

/**
 *  代码沙箱接口
 */
public interface CodeSandBox {


        ExecuteCodeResponse executeCode(ExecuteCodeRequest executeCodeRequest);
}
