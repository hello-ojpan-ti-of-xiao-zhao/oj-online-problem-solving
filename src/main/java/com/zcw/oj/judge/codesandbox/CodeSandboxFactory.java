package com.zcw.oj.judge.codesandbox;

import com.zcw.oj.judge.codesandbox.impl.ExampleCodeSandBox;
import com.zcw.oj.judge.codesandbox.impl.RemoteCodeSandBox;
import com.zcw.oj.judge.codesandbox.impl.ThirdPartyCodeSandBox;

/**
 *  代码沙箱工厂（根据字符串参数创建指定的代码沙箱实例）
 */
public class CodeSandboxFactory {

    public static CodeSandBox newInstance(String type){
        switch (type){
            case "example":
                return  new ExampleCodeSandBox();
            case "remote":
                return new RemoteCodeSandBox();
            case "thirdPart":
                return new ThirdPartyCodeSandBox();
            default:
                return new ExampleCodeSandBox();

        }
    }
}
