package com.zcw.oj.judge.strategy;

import com.zcw.oj.model.dto.question.JudgeCase;
import com.zcw.oj.judge.codesandbox.model.JudgeInfo;
import com.zcw.oj.model.entity.Question;
import com.zcw.oj.model.entity.QuestionSubmit;
import lombok.Data;

import java.util.List;

@Data
public class JudgeContext {

    private JudgeInfo judgeInfo;

    private List<String> inputList;

    private List<String> outputList;

    private List<JudgeCase> judgeCaseList;

    private Question question;

    private QuestionSubmit questionSubmit;
}
