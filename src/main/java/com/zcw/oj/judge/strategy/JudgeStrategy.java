package com.zcw.oj.judge.strategy;

import com.zcw.oj.judge.codesandbox.model.JudgeInfo;

/**
 *  判题 策略
 */
public interface JudgeStrategy {

    JudgeInfo doJudge(JudgeContext judgeContext);
}
